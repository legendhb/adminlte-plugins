<?php

namespace Biqu\adminltePlugins;

use yii\web\AssetBundle;

/**
 * @author LegendHB<legendhb@gmail.com>
 */
class BaseAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';

    public $language = 'zh-CN';
}
