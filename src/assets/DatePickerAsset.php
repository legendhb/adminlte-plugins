<?php

namespace Biqu\adminltePlugins\assets;

use Biqu\adminltePlugins\BaseAsset;

/**
 * @author LegendHB<legendhb@gmail.com>
 */
class DatePickerAsset extends BaseAsset
{

    public $js = [
        'datepicker/bootstrap-datepicker.js',
    ];
    public $css = [
        'datepicker/datepicker3.css',
    ];

    public function init(){

        $this->js[] = "datepicker/locales/bootstrap-datepicker.{$this->language}.js";
        parent::init();
    }
}
