<?php

namespace Biqu\adminltePlugins\assets;

use Biqu\adminltePlugins\BaseAsset;

/**
 * @author LegendHB<legendhb@gmail.com>
 */
class CkEditorAsset extends BaseAsset
{

    public $js = [
        'ckeditor/ckeditor.js',
    ];

}
