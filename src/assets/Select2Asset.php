<?php

namespace Biqu\adminltePlugins\assets;

use Biqu\adminltePlugins\BaseAsset;

/**
 * @author LegendHB<legendhb@gmail.com>
 */
class Select2Asset extends BaseAsset
{

    public $js = [
        'select2/select2.full.min.js',
    ];
    public $css = [
        'select2/select2.min.css',
    ];

    public function init(){

        $this->js[] = "select2/i18n/{$this->language}.js";
        parent::init();
    }
}
