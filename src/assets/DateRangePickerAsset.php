<?php

namespace Biqu\adminltePlugins\assets;

use Biqu\adminltePlugins\BaseAsset;

/**
 * @author LegendHB<legendhb@gmail.com>
 */
class DateRangePickerAsset extends BaseAsset
{

    public $js = [
        'daterangepicker/moment.min.js',
        'daterangepicker/daterangepicker.js',
    ];
    public $css = [
        'daterangepicker/daterangepicker.css',
    ];

    public function init(){
        parent::init();
    }
}
